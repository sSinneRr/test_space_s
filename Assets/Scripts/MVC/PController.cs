﻿using System.Collections;
using System.Collections.Generic;
using System;


public class PController  {
    public PModel model { get; private set; }
    public Pview view { get; private set; }

    public PController(PModel model, Pview view)
    {
        this.model = model;
        this.view = view;

        this.model.OnPositionChanged += OnPositionChanged;
    }

    private void OnPositionChanged(Vector3 position)
    {
        
        Vector3 pos = this.model.position;

       
        this.view.SetPosition(new UnityEngine.Vector3(pos.x, pos.y, pos.z));
    }
    
   
    private void SetPosition(Vector3 position)
    {
        this.model.position = position;
    }
}
