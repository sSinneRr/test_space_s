﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Manager : MonoBehaviour
{
    private void Awake() {
        LoadPlayer();
    }
   
    [ContextMenu("Load Player")]
    private void LoadPlayer() {
        new PFactory().Load();
    }
    
}
