﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PFactory  {
 public PController controller {get; private set;}
 public Pview view {get; private set;}
 public PModel model {get; private set;}

    public void Load()
    {
        GameObject prefab = Resources.Load<GameObject>("Player");
        GameObject instance = GameObject.Instantiate<GameObject>(prefab);
        this.model = new PModel();
        this.view = instance.GetComponent<Pview>();
        this.controller = new PController(model, view);
    }
    public void Move() {
    }
}
